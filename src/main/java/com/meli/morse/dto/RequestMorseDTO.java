package com.meli.morse.dto;

public class RequestMorseDTO {
    public String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
