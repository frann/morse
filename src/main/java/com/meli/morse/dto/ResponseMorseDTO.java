package com.meli.morse.dto;

public class ResponseMorseDTO {
    public String code;
    public String response;

    public String getCode() {
        return code;
    }

    public String getResponse() {
        return response;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
