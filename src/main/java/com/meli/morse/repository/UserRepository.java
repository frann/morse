package com.meli.morse.repository;

import com.meli.morse.model.User;
import org.springframework.data.repository.Repository;

import java.util.List;

public interface UserRepository extends Repository<User, Long> {
    List<User> findAll();
    User save(User user);
}
