package com.meli.morse.repository;

import com.meli.morse.model.Character;
import org.springframework.data.repository.Repository;

import java.util.List;

public interface CharacterRepository extends Repository<Character, Long> {
    List<Character> findAll();
}
