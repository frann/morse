package com.meli.morse.controller;

import com.meli.morse.dto.RequestMorseDTO;
import com.meli.morse.dto.ResponseMorseDTO;
import com.meli.morse.service.TranslateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("*")
@RestController
public class TranslateController {

    @Autowired
    private TranslateService translateService;

    @RequestMapping(path = "/health", method = RequestMethod.GET)
    public String health() {
        return "ok";
    }

    @RequestMapping(path = "/translate/bits2Morse", method = RequestMethod.POST)
    public String bits2Morse(@RequestParam("bits") String bits) {
        return translateService.decodeBits2Morse(bits);
    }

    @RequestMapping(value = "/translate/morse2Human", produces = "application/json", method=RequestMethod.POST)
    public ResponseEntity<ResponseMorseDTO> morse2Human(@RequestBody RequestMorseDTO morseJson) {
        ResponseMorseDTO response = new ResponseMorseDTO();
        response.setResponse(translateService.translate2Human(morseJson.getText()));
        response.setCode("200");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/translate/human2Morse", produces = "application/json", method=RequestMethod.POST)
    public ResponseEntity<ResponseMorseDTO> human2Morse(@RequestBody RequestMorseDTO morseJson) {
        ResponseMorseDTO response = new ResponseMorseDTO();
        response.setResponse(translateService.translateHuman2Morse( morseJson.getText()));
        response.setCode("200");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}