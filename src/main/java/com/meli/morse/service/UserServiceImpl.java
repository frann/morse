package com.meli.morse.service;

import com.meli.morse.model.User;
import com.meli.morse.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;



    @Override
    public List<User> getAll() {
        return this.userRepository.findAll();
    }

    @Override
    public User searchById(Long id) {
        return null;
    }

    @Override
    public User add(User user) {
        return this.userRepository.save(user);
    }

    @Override
    public User edit(User user) {
        return null;
    }

    @Override
    public User delete(Long id) {
        return null;
    }
}
