package com.meli.morse.service;

import com.meli.morse.model.Character;

import java.util.List;

public interface CharacterService {
    List<Character> getAll();
}
