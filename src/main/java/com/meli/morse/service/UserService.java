package com.meli.morse.service;

import com.meli.morse.model.User;

import java.util.List;

public interface UserService {
    List<User> getAll();
    User searchById(Long id);
    User add(User user);
    User edit(User user);
    User delete(Long id);
}
