package com.meli.morse.service;

import com.meli.morse.model.Character;
import com.meli.morse.repository.CharacterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CharacterServiceImpl implements CharacterService {
    @Autowired
    private CharacterRepository characterRepository;

    @Override
    public List<Character> getAll() {
        return this.characterRepository.findAll();
    }

}
