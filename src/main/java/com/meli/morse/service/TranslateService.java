package com.meli.morse.service;

public interface TranslateService {
    String decodeBits2Morse(String bits);
    String translate2Human(String morse);
    String translateHuman2Morse(String s);
}
