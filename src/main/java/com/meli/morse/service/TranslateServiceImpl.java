package com.meli.morse.service;

import com.meli.morse.model.Character;
import com.meli.morse.repository.CharacterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


@Service
public class TranslateServiceImpl implements TranslateService {

    private Map<String, String> characters;

    @Autowired
    private CharacterRepository characterRepository;

    @PostConstruct
    public void init(){
        loadCharacters();
    }

    @Override
    public String decodeBits2Morse(String bits) {
        String result = "";
        return result;
    }

    @Override
    public String translate2Human(String morse) {
        String result = "";
        List<String> stringList = Pattern.compile(" ")
                .splitAsStream(morse)
                .collect(Collectors.toList());

        for (String s : stringList){
            result += getKeyFromValue(s);
        }

        return result;
    }

    @Override
    public String translateHuman2Morse(String text) {
        String result = "";

        for (java.lang.Character c : text.toCharArray()){
            if (" ".equals(c.toString()))
                result += " ";
            else
                result += characters.get(c.toString()) + " ";
        }

        return result;
    }

    public void loadCharacters(){
        List<Character> chars = characterRepository.findAll();
        characters = new HashMap<>();

        for (Character c : chars) {
            characters.put(c.getCharacterName(), c.getCharacterValue());
        }
    }

    public String getKeyFromValue(String value) {
        return characters.entrySet().stream()
                .filter(e -> e.getValue().equals(value))
                .map(Map.Entry::getKey)
                .findFirst()
                .orElse("#");
    }
}
