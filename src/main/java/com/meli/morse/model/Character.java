package com.meli.morse.model;

import javax.persistence.*;

@Entity
@Table(name = "CHARACTERS")
public class Character {
    @Id
    @Column
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long characterId;

    @Column
    private String characterName;

    @Column
    private String characterValue;

    public Long getCharacterId() {
        return characterId;
    }

    public String getCharacterName() {
        return characterName;
    }

    public String getCharacterValue() {
        return characterValue;
    }

    public void setCharacterId(Long characterId) {
        this.characterId = characterId;
    }

    public void setCharacterName(String characterName) {
        this.characterName = characterName;
    }

    public void setCharacterValue(String characterValue) {
        this.characterValue = characterValue;
    }
}
